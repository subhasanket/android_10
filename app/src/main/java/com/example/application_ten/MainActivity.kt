package com.example.application_ten

import android.content.res.AssetFileDescriptor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.MediaPlayer
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import java.io.InputStream


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btn_read_txt.setOnClickListener {
            try {
                val inputStream: InputStream = assets.open("text_file")
                val inputString = inputStream.bufferedReader().use { it.readText() }
                txt_view.text = inputString
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        btn_read_img.setOnClickListener {
            try {
                var inputstream: InputStream = assets.open("ic_trophy.png")
                var bitmap: Bitmap = BitmapFactory.decodeStream(inputstream)
               image_view.setImageBitmap(bitmap)
            }catch (e:Exception)
            {
                e.printStackTrace()
            }
        }
        btn_play.setOnClickListener {
            val p = MediaPlayer()
            try {
                val afd: AssetFileDescriptor = applicationContext.assets.openFd("music.mp3")
                p.setDataSource(afd.fileDescriptor, afd.startOffset, afd.length)
                afd.close()
                p.prepare()
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
            p.start()
        }
    }
}
